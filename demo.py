# coding=utf-8
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from threading import * #Timer, Thread
import telegram
import sqlite3
import logging
from time import sleep
import random
import copy
import httplib2
from oauth2client.service_account import ServiceAccountCredentials
import random

from settings import *
from datetime import datetime, timedelta
from pytz import timezone
import json



def start(bot, update):
    uid = update.message.chat_id
    print('start', uid)
    t = 'Привет! Это бот пиратского радио Шалаш FM.\n\n' \
        'Он создан, чтобы сделать нас ближе. Мы хотим услышать тебя.\n\n' \
        'Новости и расписание в нашем Instagram:\nhttps://www.instagram.com/shalash_fm/\n\n' \
        'Твой Шалаш FM'
    rm = telegram.ReplyKeyboardMarkup(active_keyboard, resize_keyboard=True)
    bot.send_chat_action(chat_id=uid, action=telegram.ChatAction.TYPING)
    sleep(DELAY_TYPING)
    bot.send_message(chat_id=uid, text=t, parse_mode=telegram.ParseMode.HTML, reply_markup=rm)

    t1 = "Шалаш FM на даче\n\nМир уже никогда не будет прежним, нас разбросало по разным уголкам вселенной, " \
         "но мы всё еще рядом и хотим чувствовать это.\n" \
         "Шалаш FM из любви и палок остаётся тем самым местом из детства, куда ты можешь пригласить того, " \
         "кто тебе нравится и пошептаться о самом заветном. Это наш дом, полный тепла, душевных разговоров, " \
         "объятий и любимых людей. Где бы мы ни были, мы сами создаем свой дом.\n\n" \
         "10-12 февраля состоится Lampu, и мы снова построим свой дом уже на Бали.\n" \
         "Мы хотим слышать ваши голоса, разделить ваши чувства и эмоции, где бы вы не были, увидеть всех вас " \
         "через ваше представление о Доме. Из ваших голосов будет создан бесконечный трек, которым мы поделимся с " \
         "комьюнити и, возможно, в сложные минуты, он станет для вас поддержкой.\n\n" \
         "Запишите голосовое с ответом на один вопрос:\n" \
         "Что такое Дом для вас?\n\nотправь мне войс до 8 февраля включительно 😘"
    bot.send_chat_action(chat_id=uid, action=telegram.ChatAction.TYPING)
    sleep(DELAY_TYPING)
    bot.send_message(chat_id=uid, text=t1, parse_mode=telegram.ParseMode.HTML)

    with open('users.txt', 'a') as file:
        t = str(uid) + '\n'
        file.write(t)



def react_inline(bot, update):
    query = update.callback_query
    mid = query.message.message_id
    print("QUERY:", query.data)
    uid = query.from_user['id']

    if 'cancel' in query.data:
        pass



def got_voice(bot, update):
    uid = update.message.from_user.id
    mid = update.message.message_id
    print("VOICE:", uid, update.message.message_id, update.message.from_user.username)
    if uid < 0:
        return
    else:
        for a in admin_list:
            bot.forward_message(chat_id=a, from_chat_id=uid, message_id=mid)
        bot.send_message(chat_id=update.message.chat_id, text="Отправил в редакцию, спасибушки!")



def echo(bot, update):
    uid = update.message.from_user.id
    cid = update.message.chat_id
    print("USER:", uid, update.message.text, update.message.message_id, update.message.from_user.username)
    t = ''
    t_in = update.message.text
    if uid < 0:
        return
    if t_in == "300":
        bot.send_message(chat_id=update.message.chat_id, text="Ты сам знаешь, что делать")
    elif t_in == 'Отправить войс':
        t = 'Отправляй скорее, чего же ты ждешь?'
    else:
        t = "Я сейчас не жду никаких команд\nЕсли не знаешь, зачем этот бот - жми команду /about"
    if t:
        bot.send_message(chat_id=update.message.chat_id, text=t)




if __name__ == '__main__':
    dispatcher = updater.dispatcher
    # dispatcher.add_handler(MessageHandler(Filters.text, echo))
    dispatcher.add_handler(MessageHandler(Filters.text, echo)) # & ~Filters.command, echo))
    dispatcher.add_handler(MessageHandler(Filters.voice, got_voice))

    dispatcher.add_handler(CallbackQueryHandler(react_inline))

    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("about", start))

    updater.start_polling()

    print('start_shalash')


